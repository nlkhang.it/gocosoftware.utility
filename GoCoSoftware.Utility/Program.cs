﻿using System;
using System.Collections.Generic;

namespace GoCoSoftware.Comparer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // guid
            var guid1 = ComparisonHelper.CompareObjectSameType(Guid.NewGuid(), Guid.NewGuid());
            var guidTest = Guid.NewGuid();
            var guid2 = ComparisonHelper.CompareObjectSameType(guidTest, guidTest);

            // string
            var string1 = ComparisonHelper.CompareObjectSameType("Testing", "Testing1");
            var string2 = ComparisonHelper.CompareObjectSameType("Testing", "Testing");

            // int
            var int1 = ComparisonHelper.CompareObjectSameType(1, 2);
            var int2 = ComparisonHelper.CompareObjectSameType(2, 2);

            // int?
            int? a = 1;
            int? b = 2;
            var int3 = ComparisonHelper.CompareObjectSameType(a, b);

            int? c = 1;
            int? d = null;
            var int4 = ComparisonHelper.CompareObjectSameType(c, d);

            int? e = null;
            int? f = null;
            var int5 = ComparisonHelper.CompareObjectSameType(e, f);

            // Datetime
            var datetime1 = ComparisonHelper.CompareObjectSameType(DateTime.UtcNow, DateTime.UtcNow);

            var date1 = DateTime.UtcNow;
            var date2 = date1;
            var datetime2 = ComparisonHelper.CompareObjectSameType(date1, date2);

            // object failed
            var guid = Guid.NewGuid();

            var auditLog1 = new AuditLog()
            {
                CorrelationId = guid,
                CreatedAt = DateTime.UtcNow,
                OperationCategory = 1,
                ProtectionLevel = 1,
                RequestIp = "testing"
            };

            var auditLog2 = new AuditLog()
            {
                CorrelationId = guid,
                CreatedAt = DateTime.UtcNow,
                OperationCategory = 1,
                ProtectionLevel = 1,
                RequestIp = "testing"
            };

            var result3 = ComparisonHelper.CompareObjectSameType(auditLog1, auditLog2);

            var date = DateTime.UtcNow;

            var guidTest1 = Guid.NewGuid();
            var guidTest2 = Guid.NewGuid();
            var guidTest3 = Guid.NewGuid();
            var guidTest4 = Guid.NewGuid();

            var auditLog3 = new AuditLog()
            {
                CorrelationId = guid,
                CreatedAt = date,
                OperationCategory = 1,
                ProtectionLevel = 1,
                RequestIp = "testing",
                AffectedObject = new AffectedObjectModel
                {
                    Uuid = guid,
                    NodeType = 10
                },
                ClientIds = new[] { "Test1", "Test2" },
                Purposes = new[] { 1, 2, 10 },
                AffectedPersonIds = new HashSet<Guid>() { guidTest1, guidTest2, guidTest3 },
                AffectedObjects = new List<AffectedObjectModel>
                {
                    new AffectedObjectModel()
                    {
                        Uuid = guidTest1,
                        NodeType = 10
                    },
                    new AffectedObjectModel()
                    {
                        Uuid = guidTest1,
                        NodeType = 10
                    }
                }
            };

            var auditLog4 = new AuditLog()
            {
                CorrelationId = guid,
                CreatedAt = date,
                OperationCategory = 1,
                ProtectionLevel = 1,
                RequestIp = "testing",
                AffectedObject = new AffectedObjectModel
                {
                    Uuid = guid,
                    NodeType = 10
                },
                ClientIds = new[] { "Test1", "Test2" },
                Purposes = new[] { 1, 2, 10 },
                AffectedPersonIds = new HashSet<Guid>() { guidTest1, guidTest2, guidTest3 },
                AffectedObjects = new List<AffectedObjectModel>
                {
                    new AffectedObjectModel()
                    {
                        Uuid = guidTest1,
                        NodeType = 10
                    },
                    new AffectedObjectModel()
                    {
                        Uuid = guidTest1,
                        NodeType = 10
                    }
                }
            };

            var result4 = ComparisonHelper.CompareObjectSameType(auditLog3, auditLog4);
        }
    }

    public class AuditLog
    {
        public Guid CorrelationId { get; set; }

        public string RequestIp { get; set; }

        public int OperationCategory { get; set; }

        public DateTime CreatedAt { get; set; }

        public int? ProtectionLevel { get; set; }

        public AffectedObjectModel AffectedObject { get; set; }

        public string[] ClientIds { get; set; }

        public int[] Purposes { get; set; }

        public HashSet<Guid> AffectedPersonIds { get; set; }

        public IEnumerable<AffectedObjectModel> AffectedObjects { get; set; }
    }

    public class AffectedObjectModel
    {
        public Guid Uuid { get; set; }

        public int NodeType { get; set; }
    }
}
