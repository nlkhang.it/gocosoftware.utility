﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace GoCoSoftware.Comparer
{
    public static class ComparisonHelper
    {
        public static bool CompareObjectSameType(object object1, object object2)
        {
            if (object1 == null && object2 == null)
            {
                return true;
            }

            if (object1 == null && object2 != null || object1 != null && object2 == null)
            {
                return false;
            }

            if (object1.GetType() != object2.GetType())
            {
                return false;
            }

            // simple type
            if (IsSimpleType(object1.GetType()))
            {
                return object1.Equals(object2);
            }

            // object type
            if (IsObjectType(object1))
            {
                var propertiesObject1 = object1.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (var property in propertiesObject1)
                {
                    var propertyValueObject1 = property.GetValue(object1);
                    var propertyValueObject2 = object2.GetType().GetProperty(property.Name).GetValue(object2);

                    if (!CompareObjectSameType(propertyValueObject1, propertyValueObject2))
                    {
                        return false;
                    }
                }
            }

            // collection type
            if (object1 is IEnumerable)
            {
                var listObject1 = (object1 as IEnumerable).Cast<object>();
                var listObject2 = (object2 as IEnumerable).Cast<object>();

                if (listObject1.Count() != listObject2.Count())
                {
                    return false;
                }

                // compare sequence list item
                for (int i = 0; i < listObject1.Count(); i++)
                {
                    var collectionItem1 = listObject1.ElementAt(i);
                    var collectionItem2 = listObject2.ElementAt(i);

                    if (!CompareObjectSameType(collectionItem1, collectionItem2))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private static bool IsSimpleType(Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                // nullable type, check if the nested type is simple.
                return IsSimpleType(type.GetGenericArguments()[0].GetTypeInfo());
            }

            return type.IsPrimitive
              || type.IsValueType
              || type.IsEnum
              || type.Equals(typeof(string))
              || type.Equals(typeof(decimal));
        }

        private static bool IsObjectType(object obj)
        {
            var typeCode = Type.GetTypeCode(obj.GetType());
            return typeCode == TypeCode.Object && !(obj is IEnumerable);
        }
    }
}
